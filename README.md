<!--
SPDX-FileCopyrightText: 2022 Andrea Laisa

SPDX-License-Identifier: CC0-1.0
-->

# esercizio-trasparenza-software-sorgente-aperto

Mini esercizio per comprendere la differenza di trasparenza tra un software a sorgente aperto e uno chiuso.

# Comandi per compilare tutto
```
go build -o track-server track-server.go
go build -o calcolatrice calcolatrice.go
GOOS=windows go build -o calcolatrice.exe calcolatrice.go

pdflatex esercizio.tex
```
