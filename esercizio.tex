% SPDX-FileCopyrightText: 2022 Andrea Laisa
%
% SPDX-License-Identifier: CC-BY-SA-4.0

\documentclass{article}
\usepackage[T1]{fontenc}
\usepackage[italian]{babel}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{graphicx}
\graphicspath{{immagini/}}

\begin{document}
	\author{Andrea Laisa}
	\title{[L3] Capire la differenza di trasparenza tra un software a sorgente aperto e chiuso}
	\maketitle
	\tableofcontents
	
	\section{Introduzione}
		L'obiettivo di questo esercizio è quello di far comprendere la differenza di trasparenza che c'è tra un software a sorgente aperto ovvero di cui disponiamo il relativo codice sorgente\footnote{Il codice sorgente di un programma è un testo che definisce il comportamento del programma. Esso è scritto in un determinato linguaggio di programmazione ed è intellegibile per gli umani.} e di un software di cui non abbiamo il codice sorgente. Attraverso l'uso di un computer viene proposto di provare un programma\footnote{I programmi distribuiti, per ragioni di efficienza, sono tipicamente nella forma di codice binario, ovvero in una forma che è facilmente comprensibile da una CPU ma che è di difficile interpretazione per gli umani. Tale forma viene prodotta dagli compilatori che trasformano un codice sorgente in un codice binario.} di esempio per poi capirne il funzionamento con e senza il codice sorgente, giungendo alla conclusione che un software aperto è più trasparente di uno chiuso.
		
	\section{Informazioni}
		\begin{description}
			\item[livello arcobaleno tecnocivismo] L3 - educazione
			\item[destinatari] chiunque, con un minimo di elasticità mentale
			\item[keyword] trasparenza, sorgente aperto, sorgente chiuso, software
			\item[durata] 5/10 minuti
			\item[difficoltà] 2/10
			\item[strumenti necessari] un computer con GNU/Linux o Windows
			\item[costi] totalmente gratuito
		\end{description}
	
	\section{Istruzioni}
		\subsection{Preparazione per l'esercizio}
			Prima di tutto in base al sistema operativo che si usa, bisogna scaricare il programma di esempio dell'esercizio che si chiama semplicemente \emph{calcolatrice} e lanciarlo.
			
			\paragraph{Windows}
				Con un qualunque browser\footnote{Edge è sconsigliato perché mostra degli avvisi di sicurezza difficili da scavalcare.} scaricare il file da \url{https://amreowww.duckdns.org/calcolatrice.exe}. Una volta che lo scaricamento è finito, aprire il programma. A questo punto dovrebbe aprirsi una innocua finestra di terminale (una finestra nera con scritte bianche). Nel caso che il sistema operativo\footnote{Se compare un avviso "PC protetto da windows" bisogna cliccare sul collegamento "Ulteriori informazioni" per poi cliccare sul pulsante "Esegui comunque"}, l'antivirus o il browser mostra un avviso\footnote{L'avviso è dovuto semplicemente al fatto che si sta eseguendo un programma scaricato da internet} di sicurezza, accettarlo e ignorarlo.
				
			\paragraph{GNU/Linux}
				Con un qualunque browser scaricare il file da \url{https://amreowww.duckdns.org/calcolatrice}. Una volta che lo scaricamento è finito, abilitare il permesso di esecuzione del programma dal gestore file\footnote{Provato solo con thunar, ma penso che si faccia così anche con nautilus. Non è stato provato con Nemo, dolphin o altri gestori di file. Al limite si può usare il comando \emph{chmod +x ./calcolatrice}.} cliccando sulla spunta "permesso d'esecuzione" presente nella scheda permessi in proprietà file.
				Quindi aprire il terminale, andare nella cartella dove è stato scaricato (probabilmente con \verb!cd ~/Scaricati!) e lanciarlo con \verb!./calcolatrice! .
			
			\includegraphics[width=\textwidth]{immagini/fig-calcolatrice-inzio.png}
		\subsection{Capire il funzionamento del programma}
			Una volta che il programma è in esecuzione provare ad usarlo come indicato semplicemente dal programma stesso, e da ciò provare a dedurne il funzionamento.
			
			\includegraphics[width=\textwidth]{immagini/fig-calcolatrice-fine.png}
			\newpage
			
			In linea generale l'idea di funzionamento che si è dedotto dovrebbe essere del tipo:
			\begin{enumerate}
				\item Viene mostrato un messaggio di benvenuto
				\item Viene chiesto all'utente di inserire l'operatore, il primo operando e il secondo operando
				\item Viene mostrato il risultato
				\item Il programma rimane in attesa della pressione del tasto invio prima di chiudersi
				\item Il programma si chiude
			\end{enumerate}
		\subsection{Leggere il codice sorgente del programma}
			Con un qualunque browser aprire \href{https://gitlab.com/amreo/esercizio-trasparenza-software-sorgente-aperto/-/blob/main/calcolatrice.go}{il codice sorgente del programma}, provare a leggerlo (la parte rilevante comincia da \verb!func main()!) e cercare di capire cosa faccia. È garantito che il codice sorgente indicato corrisponde a quello del programma provato. Si nota qualcosa di strano o sorprendente?
			\includegraphics[width=\textwidth]{immagini/codice-sorgente.png}
			\newpage
			
			\newpage
			La risposta è sì. Probabilmente si è notato che nel codice sorgente sono presenti le seguenti linee sospette:
			
			\begin{lstlisting}[language=Go,breaklines]
// Invia i dati del calcolo (operazione inserita, operando1, operando2) e l'username dell'utente al server di tracciamento
// I dati inviati dei calcoli possono essere visualizzati qui: https://trackserver.duckdns.org
u, _ := user.Current()
url := fmt.Sprintf("https://trackserver.duckdns.org/add?username=%s&val1=%d&val2=%d&op=%s", u.Username, operando1, operando2, url.QueryEscape(operazioneInserita))
http.Get(url)
			\end{lstlisting}
			
			Queste linee di codice come spiegato nei codice inviano dei dati a \url{https://trackserver.duckdns.org}. È consigliato prenderne visione per vedere i dati di tutti coloro che hanno eseguito il programma. Viene fatto osservare che questo comportamento non è stato notato durante l'uso del programma, comportamento che è malevolo per l'utente.
			
			\includegraphics[width=\textwidth]{immagini/trackserver.png}
	\section{Conclusioni e possibili mitigazioni}
		Giunti qui, si deve avere osservato e imparato che:
		\begin{itemize}
			\item non si può dedurre completamente e correttamente il comportamento di un programma dalle sue interazioni con l'utente e con l'esterno.
			\item il codice sorgente di un software, posto che corrisponde veramente al programma(e versione) in questione, non mente.
			\item un software a sorgente aperto è più trasparente di uno chiuso.
			\item leggendo il codice sorgente di un software, si può avere la assoluta certezza di quello che fa, e quindi se sia completamente benevolo o un po' malevolo.
		\end{itemize}
		
		Quindi, per evitare brutte sorprese ovvero la presenza di funzionalità non desiderabili (in gergo anti feature), è consigliato preferire sempre software che sono perlomeno completamente \emph{source-available}, cioè di cui si abbia la possibilità di avere a disposizione il relativo codice sorgente. 

		Se il software è libero, possiamo usufruire della libertà (1), ovvero quella di modifica per rimuovere tutte le impurità dal software e la libertà (3) per ridistribuirne i miglioramenti.
			
	\section{Vedi anche}
		\begin{description}
			\item[software libero] Il \href{https://www.gnu.org/philosophy//free-sw.it.html}{software libero} è software che rispetta la libertà degli utenti e la comunità. In breve, significa che gli utenti hanno la libertà di eseguire, copiare, distribuire, studiare, modificare e migliorare il software.
			\item[build riproducibili] le \href{https://reproducible-builds.org/}{build riproducibili} sono delle pratiche di sviluppo software che hanno l'obiettivo di garantire che un dato codice binario corrisponde a un dato codice sorgente.
		\end{description}
	\section{Note}
		\begin{itemize}
			\item Chiaramente siete stati aiutati a comprendere il codice sorgente. Nella realtà il codice sorgente è più difficile da comprendere in quanto ci potrebbero essere moltissime linee di codice, essere scarsamente documentato, incasinato e avere un elevato numero di dipendenze.
			\item È possibile scoprire il comportamento di un programma anche tramite l'impiego dell'\href{https://it.wikipedia.org/wiki/Ingegneria_inversa}{ingegneria inversa}, ovvero l'analisi di un programma a partire dal programma compilato e/o del suo comportamento con l'esterno però non è in grado di garantire la correttezza dell'analisi, e richiede notevoli competenze informatiche e tanto tempo.
		\end{itemize}
	
	\section{Licenza e codice sorgente}
		Questo esercizio è sotto licenza CC-BY-SA. Tutti i sorgenti possono essere trovati su \url{https://gitlab.com/amreo/esercizio-trasparenza-software-sorgente-aperto}.
\end{document}