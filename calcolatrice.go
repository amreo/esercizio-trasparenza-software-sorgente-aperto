// SPDX-FileCopyrightText: 2022 Andrea Laisa
//
// SPDX-License-Identifier: GPL-3.0-only

package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os/user"
)

// Il programma inizia da qui
func main() {
	var operazioneInserita string
	var operando1 int
	var operando2 int
	var risultato int
	// Visualizza sullo schermo il messaggio iniziale del programma
	fmt.Println("**********************************")
	fmt.Println("* Calcolatrice fantastica v3.141 *")
	fmt.Println("**********************************")
	fmt.Println("Benvenuto in questa avanzatissima calcolatrice di soli numeri naturali!")

	// Chiede l'operazione che si vuole fare e salva la risposta in operazioneInserita
	fmt.Print("Che operazione vuoi fare? [+ - * /] e premere invio: ")
	fmt.Scanf("%s ", &operazioneInserita)
	// Chiede sia il primo che il secondo operando e li inserisce in operando1 e operando2
	fmt.Print("Inserire il primo operando: ")
	fmt.Scanf("%d ", &operando1)
	fmt.Print("Inserire il secondo operando: ")
	fmt.Scanf("%d ", &operando2)

	// In base all'operazione inserita per casi viene calcolato il risultato correttamente
	switch operazioneInserita {
	case "+":
		risultato = operando1 + operando2
	case "-":
		risultato = operando1 - operando2
	case "*":
		risultato = operando1 * operando2
	case "/":
		risultato = operando1 / operando2
	}
	// Visualizza sullo schermo il risultato dell'operazione
	fmt.Printf("Il risultato è %d\n", risultato)

	// Invia i dati del calcolo (operazione inserita, operando1, operando2) e l'username dell'utente al server di tracciamento
	// I dati inviati dei calcoli possono essere visualizzati qui: https://trackserver.duckdns.org
	u, _ := user.Current()
	url := fmt.Sprintf("https://trackserver.duckdns.org/add?username=%s&val1=%d&val2=%d&op=%s", u.Username, operando1, operando2, url.QueryEscape(operazioneInserita))
	http.Get(url)

	// Attende la pressione di un qualunque tasto da parte dell'utente per uscire (serve solo per windows)
	fmt.Print("Premere invio per uscire")
	fmt.Scanf("%s", &operazioneInserita)
}
