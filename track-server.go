// SPDX-FileCopyrightText: 2022 Andrea Laisa
//
// SPDX-License-Identifier: GPL-3.0-only

package main

import (
	"fmt"
	"net/http"
	"strconv"
)

type Operation struct {
	username string
	val1     int
	op       string
	val2     int
}

var operations []Operation = make([]Operation, 0)

func serveIndex(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, `
	<html>
		<a href="https://gitlab.com/amreo/esercizio-trasparenza-software-sorgente-aperto">Codice sorgente di questo programmino</a></br>
		<a href="/add?username=pippo&val1=10&val2=20&op=*">URL invio dati di esempio</a>
		<h1>Lista operazioni</h1>
		<ul>
	`)

	for _, v := range operations {
		fmt.Fprintf(w, "<li>%s ha fatto %d %s %d</li>", v.username, v.val1, v.op, v.val2)
	}
	fmt.Fprintln(w, "</ul>\n</html>")
}

func addOperation(w http.ResponseWriter, r *http.Request) {
	username, ok := r.URL.Query()["username"]
	if !ok {
		fmt.Fprintln(w, "failed")
		return
	}

	val1, ok := r.URL.Query()["val1"]
	if !ok {
		fmt.Fprintln(w, "failed")
		return
	}

	val2, ok := r.URL.Query()["val2"]
	if !ok {
		fmt.Fprintln(w, "failed")
		return
	}

	op, ok := r.URL.Query()["op"]
	if !ok {
		fmt.Fprintln(w, "failed")
		return
	}

	var newOp Operation
	newOp.username = trimUpToN(username[0], 32)
	newOp.val1, _ = strconv.Atoi(val1[0])
	newOp.val2, _ = strconv.Atoi(val2[0])
	newOp.op = trimUpToN(op[0], 1)

	operations = append(operations, newOp)
	fmt.Fprintln(w, "ok")
}

func main() {
	http.Handle("/", http.HandlerFunc(serveIndex))
	http.Handle("/add", http.HandlerFunc(addOperation))

	http.ListenAndServe(":9090", nil)
}

func trimUpToN(s string, n int) string {
	if len(s) < n {
		return s
	} else {
		return s[:n]
	}
}
